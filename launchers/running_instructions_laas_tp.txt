
###################
# Aerostack Trajectory planner
###########

# Launch
roslaunch $FUSEON_WORKSPACE/src/aerostack_substack/launchers/laas_trajectory_planner2d.launch

# start
rosservice call /fuseon/drone0/droneTrajectoryPlanner/start "{}" 


# Publish mission point
rostopic pub -1 /fuseon/drone0/droneMissionPoint droneMsgsROS/dronePositionRefCommand -- 5.0 1.0 1.25
rostopic pub -1 /fuseon/drone0/droneMissionPoint droneMsgsROS/dronePositionRefCommand -- 1.0 4.0 1.25
rostopic pub -1 /fuseon/drone0/droneMissionPoint droneMsgsROS/dronePositionRefCommand -- -1.0 4.0 1.25





###################
# Aerostack Trajectory planner interface
###########

# 
roslaunch $FUSEON_WORKSPACE/src/aerostack_substack/launchers/laas_trajectory_planner2d_interface.launch 



